
//
//  CVViewController.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVAlbumViewController: CVBaseViewController {
    
    var albums = [URL]()

    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        albums = albumsDirectory.getContentsOfDirectory()
        addAdditionalHeight = true
    }
    
    // MARK: - collection methods
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        albums.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCollectionViewCell.identifier, for: indexPath) as! CVCollectionViewCell
        cell.configureCell(with: albums[indexPath.row])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewAlbumVC = storyboard?.instantiateViewController(withIdentifier: CVViewAlbumController.identifier) as? CVViewAlbumController {
            viewAlbumVC.setViewAlbum(with: albums[indexPath.row])
            viewAlbumVC.delegate = self
            navigationController?.pushViewController(viewAlbumVC, animated: true)
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func didTapAddButton(_ sender: Any) {
        let addAlbumAC = UIAlertController(title: "New Album", message: "Add New Album", preferredStyle: .alert)
        setAlertAction(for: addAlbumAC)
        self.present(addAlbumAC, animated: true)
    }
    
    // MARK: - Custom Methods
    
    func setAlertAction(for alertController: UIAlertController) {
        // Alert Actions Buttons
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        let saveButton = UIAlertAction(title: "Save", style: .default, handler: { [weak self] action in
            // Album Name textField
            if let name = alertController.textFields?[0].text {
                self?.createNewAlbum(name: name)
            }
            self?.collectionView.reloadData()
        })
        // Save Button is enabled only when text is not empty
        alertController.addTextField { (textField) in
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using:
                {_ in
                    let textCount = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
                    let textIsNotEmpty = textCount > 0
                    saveButton.isEnabled = textIsNotEmpty
            })
        }
        saveButton.isEnabled = false
        // Add alert action button to alert controller
        alertController.addAction(cancelButton)
        alertController.addAction(saveButton)
    }
    
    func createNewAlbum(name: String) {
        let newAlbum = albumsDirectory.appendingPathComponent(name)
        if !FileManager.default.fileExists(atPath: newAlbum.path) {
            do {
                try FileManager.default.createDirectory(atPath: newAlbum.path, withIntermediateDirectories: true, attributes: nil)
                albums.append(newAlbum)
            } catch {
                alertUser(on: "Error", with: error.localizedDescription)
            }
        } else {
            alertUser(on: "Rename Folder", with: "Album name already exists")
        }
    }
    
}

extension CVAlbumViewController: CVViewAlbumControllerDelegate {
    
    func didUpdateAlbum() {
        collectionView.reloadData()
    }
    
}
