//
//  CVBaseViewController.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 26/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVBaseViewController: UICollectionViewController {

    let albumsDirectory = FileManager.getAlbumsDirectory()
    var width: CGFloat = 300
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var addAdditionalHeight = false
    
    // MARK: - View lifecycle method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupCollectionViewItemSize(collectionViewFlowLayout: collectionViewFlowLayout)
        collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Custom function
    
    func setupCollectionViewItemSize(collectionViewFlowLayout: UICollectionViewFlowLayout) {
        
        var numberOfItemsInRow: CGFloat! = 5
        let lineSpacing: CGFloat = 8
        let interItemSpacing: CGFloat = 8
        var layoutMarginsLeft: CGFloat = 8
        
        if let firstWindow = UIApplication.shared.windows.first {
            if let windowScene = firstWindow.windowScene {
                if windowScene.interfaceOrientation.isPortrait && UIDevice.current.userInterfaceIdiom == .phone {
                    numberOfItemsInRow = 3
                }
            }
        }
        
        if let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
            layoutMarginsLeft = window.layoutMargins.left
        }
        
        let width: CGFloat = ((collectionView.frame.width - (interItemSpacing * (numberOfItemsInRow - 1)) - (layoutMarginsLeft * 2)) / numberOfItemsInRow)
        let height: CGFloat
        self.width = width
        
        if addAdditionalHeight {
            // add additional height for albums grid
            height = width + 30
        } else {
            height = width
        }
        
        collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
        collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
        collectionViewFlowLayout.minimumLineSpacing = lineSpacing
        collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
        collectionViewFlowLayout.sectionInsetReference = .fromLayoutMargins
    }
    
    func alertUser(on title: String, with message: String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.title = title
        alert.message = message
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        present(alert, animated: true)
    }
}
