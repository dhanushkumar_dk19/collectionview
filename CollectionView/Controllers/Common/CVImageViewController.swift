//
//  CVImageViewController.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 03/01/21.
//  Copyright © 2021 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVImageViewController: UIViewController {
    
    @IBOutlet weak var displayImageView: UIImageView!
    
    static let identifier = "ImageDisplay"
    
    var imagePath: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = imagePath?.lastPathComponent
        if let displayImage = imagePath {
            if FileManager.default.fileExists(atPath: displayImage.path) {
                let imageUrl = URL(fileURLWithPath: "\(displayImage.path)/original.jpg")
                displayImageView.image = CVHelper.downsample(imageAt: imageUrl, to: displayImageView.bounds.size)
            } else {
                print(DirectoryError.fileDoesNotExist)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
    
    func setImage(for image: URL) {
        imagePath = image
    }
    
}
