//
//  CVInAlbumViewController.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 24/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

protocol CVViewAlbumControllerDelegate: AnyObject {
    func didUpdateAlbum()
}

class CVViewAlbumController: CVBaseViewController {
    
    static let identifier = "viewAlbum"
    
    var navTitle: String?
    var imagePicker: UIImagePickerController?
    var currentAlbumPath: URL!
    var albumContentsPath = [URL]()
    var delegate: CVViewAlbumControllerDelegate?
    
    // MARK: - ViewLifeCycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = navTitle
        albumContentsPath = currentAlbumPath.getContentsOfDirectory()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.didUpdateAlbum()
    }
    
    // MARK: - Collection Methods
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        albumContentsPath.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CVImageViewCell.identifier, for: indexPath) as! CVImageViewCell
        cell.updateCell(with: albumContentsPath[indexPath.row])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewImage = storyboard?.instantiateViewController(withIdentifier: CVImageViewController.identifier) as? CVImageViewController {
            viewImage.setImage(for: albumContentsPath[indexPath.row])
            navigationController?.pushViewController(viewImage, animated: true)
        }
    }
    
    // MARK: - Action Method
    
    @IBAction func didTapAddImageButton(_ sender: Any) {
        if imagePicker == nil {
            imagePicker = UIImagePickerController()
            imagePicker?.delegate = self
            imagePicker?.sourceType = .photoLibrary
        }
        present(imagePicker!,animated: true)
    }
    
    // MARK: - Custom Method
    
    func setViewAlbum(with url: URL) {
        navTitle = url.lastPathComponent
        currentAlbumPath = url
    }
    
    func writeJpeg(image: UIImage,at url: URL) {
        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            do {
                try jpegData.write(to: url)
            } catch {
                alertUser(on: "Error", with: error.localizedDescription)
            }
        }
    }
    
    func dismissPicker() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - ImagePicker delegate

extension CVViewAlbumController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        dismissPicker()
        
        let imageName = UUID().uuidString
        let imagePath = currentAlbumPath.appendingPathComponent(imageName)
        
        albumContentsPath.append(imagePath)
        collectionView.reloadData()
        
        do {
            try FileManager.default.createDirectory(atPath: imagePath.path, withIntermediateDirectories: true, attributes: nil)
        } catch  {
            alertUser(on: "Error", with: error.localizedDescription)
            return
        }
        
        var originalImagePath = imagePath.appendingPathComponent("original")
        originalImagePath.appendPathExtension("jpg")
        writeJpeg(image: image, at: originalImagePath)
        
        var thumbnailUrl = imagePath.appendingPathComponent("thumbnail")
        thumbnailUrl.appendPathExtension("jpg")
        
        let thumbnailImage = CVHelper.downsample(imageAt: originalImagePath, to: CGSize(width: width, height: width))
        if let thumbnailImage = thumbnailImage {
            writeJpeg(image: thumbnailImage, at: thumbnailUrl)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissPicker()
    }
    
}

