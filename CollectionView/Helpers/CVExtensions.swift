//
//  CVExtensions.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import Foundation
import UIKit

extension FileManager {
    
    static func getAlbumsDirectory() -> URL {
        let albumUrls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let albumUrl = albumUrls.appendingPathComponent("Albums")
        try? FileManager.default.createDirectory(atPath: albumUrl.path, withIntermediateDirectories: true, attributes: nil)
        return albumUrl
    }
    
}

extension URL {
    
    func getContentsOfDirectory() -> [URL] {
        guard let currentPathContents = try? FileManager.default.contentsOfDirectory(at: self, includingPropertiesForKeys: [.creationDateKey], options: [.skipsHiddenFiles] ) else {
            return []
        }
        return currentPathContents
    }
    
}
    

