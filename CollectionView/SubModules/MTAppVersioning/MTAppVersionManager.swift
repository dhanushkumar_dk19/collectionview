//
//  MTAppVersionManager.swift
//
//  Copyright © 2017 Mallow Technologies Private Limited. All rights reserved.
//

import UIKit

let kiTunesURL = "itms-apps://itunes.apple.com/app/id"
let kVersionKey = "CFBundleShortVersionString"
let kOSType = "ios"
let kOSVersion = UIDevice.current.systemVersion

// 3 Days ~ (3 * 24 * 60 * 60)
let kAppVersionDefaultTimeIntervalFrequency: TimeInterval = 259200

let kAlertButtonTitleForForceUpdate = NSLocalizedString("Update", comment: "Update")
let kAlertButtonTitleForNormalUpdate = NSLocalizedString("Update", comment: "Update")
let kAlertButtonForLater = NSLocalizedString("Later", comment: "Later")
let kCancelButtonTitle = NSLocalizedString("Cancel", comment: "Cancel")

let kUpdateFrequency = "UpdateFrequency"
let kSceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate

enum MTAppVersionStatus: String {
    case appVersionNone = "none"
    case appVersionForce = "force"
    case appVersionUpdate = "update"
    
    static let allValues = [appVersionNone, appVersionForce, appVersionUpdate]
}

class MTAppVersionManager: NSObject {
    
    static let shared = MTAppVersionManager()
    var tintColor: UIColor? // Set custom tint color for buttons in alert.
    
    // Your app's App ID you can get this from your iTunes account in respective project, which will be used to open respective app from the AppStore.
    var appID: String?
    
    // The App versioning URL for your project.
    var appVersioningURLString: String?
    
    // Denotes when should we make next update check API call. By default 3 days time interval frequency is set. If you want customized duration update this value
    var nextUpdateFrequency: TimeInterval = kAppVersionDefaultTimeIntervalFrequency
    var alertController: UIAlertController?
    
    
    // MARK: - Initializer methods
    
    override init() {
        super.init()
        // Dummy implementation
    }
    
    /**Initialization method with appID and app version url string*/
    init(withAppID: String, andAppVersioningURLString: String) {
        super.init()
        
        MTAppVersionManager.shared.appID = withAppID
        MTAppVersionManager.shared.appVersioningURLString = andAppVersioningURLString
        
        // Add observer
        NotificationCenter.default.addObserver(self, selector: #selector(dismissAppVersionAlert), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    
    // MARK: - Custom methods
    
    /**Open the App in iTunes for update*/
    func openInAppStore() {
        let yourAppID = MTAppVersionManager.shared.appID ?? ""
        if let itunesURL: URL = URL(string: [kiTunesURL, yourAppID].joined()) {
            if UIApplication.shared.canOpenURL(itunesURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(itunesURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(itunesURL)
                }
            } else {
                // Do nothing since the URL cannot be opened.
            }
        }
    }
    
    /**Perform App versioning call*/
    func checkAppVersioning() {
        let appVersionNumber = Bundle.main.object(forInfoDictionaryKey: kVersionKey) as! String
        
        guard let appVersioningPath = MTAppVersionManager.shared.appVersioningURLString else {
            return
        }
        var urlComponents = URLComponents(string: appVersioningPath)
        
        // Add params
        let appVersion = URLQueryItem(name: "app_version", value: appVersionNumber)
        let platform = URLQueryItem(name: "platform", value: kOSType)
        let osVersion = URLQueryItem(name: "os_version", value: kOSVersion)
        let deviceModel = URLQueryItem(name: "device_model", value: UIDevice.current.modelName)
        urlComponents?.queryItems = [appVersion, platform, osVersion, deviceModel]
    
        guard let url = urlComponents?.url else {
            return
        }
        
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let responseData = data { // Success
                
                // parse the JSON reponse
                do {
                    guard let responseJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] else {
                        print("Error trying to convert data to JSON")
                        return
                    }
                    
                    // Parse the "VersionStatus" from the resposeBodyDict
                    guard let versionStatus = responseJSON["version_status"] as? [String: Any] else {
                        print("Could not get versionStatus from JSON")
                        return
                    }
                    
                    // Parse the "status" from the versionStatus
                    guard let status = versionStatus["status"] as? String else {
                        print("Could not get status from JSON")
                        return
                    }
                    
                    var message: String?
                    if let messageString = versionStatus["message"] as? String {
                        message = messageString
                    } else {
                        // Do Nothing
                    }
                    
                    self.showAlertForVersioning(forStatus: status, withMessage: message)
                } catch  {
                    print("error trying to convert data to JSON")
                    return
                }
                
            } else { // Failure
                print("Error on App version check : \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
    
    /**Check wether currentAlertController is shown and dismiss if its shown else do nothing*/
    @objc func dismissAppVersionAlert() {
        if alertController != nil {
            alertController?.dismiss(animated: true, completion: {
                self.alertController = nil
            })
        } else {
            // Do Nothing since appversion alert is not shown
        }
    }
    
    /**Check wether the app update check frequency has been reached to determine wether to show update alert to the user or not*/
    func isUpdateFrequencyReached() -> Bool {
        if let lastUpdateDate: Date = lastAppVersionDisplayTime() as Date? {
            let currentDate = Date()
            let timeInterVal = currentDate.timeIntervalSince(lastUpdateDate)
            
            if timeInterVal < 0 || timeInterVal <= nextUpdateFrequency {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    /**Show respective app versioning alert to the user based on the status received from the server*/
    func showAlertForVersioning(forStatus: String, withMessage: String?) {
        switch forStatus.lowercased() {
        case MTAppVersionStatus.appVersionNone.rawValue:
            // Do Nothing
            break
            
        case MTAppVersionStatus.appVersionForce.rawValue:
            showAlertViewForForceUpdate(withMessage)
            break
            
        case MTAppVersionStatus.appVersionUpdate.rawValue:
            if isUpdateFrequencyReached() == true {
                showAlertViewForNormalUpdate(withMessage)
            }
            break
            
        default:
            break
            //Do Nothing
        }
    }
    
    /**Normal update alert*/
    func showAlertViewForNormalUpdate(_ withMessage: String?) {
        alertController = UIAlertController(title: nil, message: withMessage, preferredStyle: .alert)
        alertController?.addAction(UIAlertAction(title: kAlertButtonTitleForForceUpdate, style: .default, handler:{ (UIAlertAction)in
            self.openInAppStore()
        }))
        alertController?.addAction(UIAlertAction(title: kAlertButtonForLater, style: .default, handler:{ (UIAlertAction)in
            self.updateLastAppVersionDisplayTime()
        }))
        configureTintColor()
        DispatchQueue.main.async {
            kSceneDelegate?.window?.rootViewController?.present(self.alertController!, animated: true, completion: nil)
        }
    }
    
    /**Force update alert*/
    func showAlertViewForForceUpdate(_ withMessage: String?) {
        alertController = UIAlertController(title: nil, message: withMessage, preferredStyle: .alert)
        alertController?.addAction(UIAlertAction(title: kAlertButtonTitleForForceUpdate, style: .default, handler:{ (UIAlertAction)in
            self.openInAppStore()
        }))
        configureTintColor()
        DispatchQueue.main.async {
            kSceneDelegate?.window?.rootViewController?.present(self.alertController!, animated: true, completion: nil)
        }
        
    }
    
    /**Configure custom tint colour of alert buttons*/
    func configureTintColor() {
        if let color = tintColor {
            alertController?.view.tintColor = color
        }
    }
    
    
    // MARK: - User default methods
    
    /**Save the date and time when we got `none` response from backend for app versioning check*/
    func updateLastAppVersionDisplayTime() {
        UserDefaults.standard.set(Date(), forKey: kUpdateFrequency)
        UserDefaults.standard.synchronize()
    }
    
    /**Get the date and time details at when we show optional update alert*/
    func lastAppVersionDisplayTime() -> Date? {
        let lastUpdateResponseTime = UserDefaults.standard.object(forKey: kUpdateFrequency) as? Date
        return lastUpdateResponseTime
    }
    
}

// MARK: - Extension returning device model name
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad7,5", "iPad7,6":                      return "iPad 6"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
