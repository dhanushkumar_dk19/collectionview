//
//  MTKeychainManager.swift
//  DigitalVault
//
//  Created by Karthick Selvaraj on 26/09/18.
//  Copyright © 2018 mallow. All rights reserved.
//

import UIKit

let kKeychainUserPassword = "\(Bundle.main.bundleIdentifier!).password" // User this key to save and access user password.
let kKeychainUserAccessToken = "\(Bundle.main.bundleIdentifier!).accessToken" // User this key to save and access JWT access token.
let kKeychainUserRefreshToken = "\(Bundle.main.bundleIdentifier!).refreshToken" // User this key to save and access JWT refresh token.

class MTKeychainManager: NSObject {

    static let sharedInstance = MTKeychainManager()
    
    var serviceName: String = Bundle.main.bundleIdentifier! // Service name is used to identify app's data in keychain. For maintaining uniqueness we are using bundle identifier as serviceName for keychain.
    
    
    // MARK: - Custom methods
    
    /**
     This methods saves the given value against given key in keychain
     
     - parameter value: Data what we are about to save in keychain.
     - parameter key: Unique key for Data what we are about to save in keychain for easy access in future
     
     - returns: True if data saved successfully. False if not saved successfully.
     */
    func save(value: String?, for key: String?) -> Bool {
        if let value = value, let key = key {
            do {
                try KeychainPasswordItem(service: serviceName, account: key).savePassword(value) // Save in Keychain.
                return true
            } catch {
                print(error)
            }
        }
        return false
    }
    
    
    /**
     This method will read the value for the given key
     
     - parameter key: Key for reading value from keychain which it associates
     
     - returns: Returns value for given key if available, else returns nil
     */
    func value(for key: String?) -> String? {
        if let key = key {
            do {
                let value = try KeychainPasswordItem(service: serviceName, account: key).readPassword() // Read value from Keychain.
                if value.count > 0 {
                    return value
                } else {
                    return nil
                }
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    /**
     Delete the value for given key from Keychain
     
     - parameter key: Key for deleting its associated value
     
     - returns: Return true if successfully deleted, false if data is not deleted or unavailable to delete
     */
    func deleteValue(for key: String?) -> Bool {
        if let key = key {
            do {
                try KeychainPasswordItem(service: serviceName, account: key).deleteItem() // Delete data from keychain.
                return true
            } catch {
                print(error)
            }
        }
        return false
    }
    
    /**Delete all saved details from Keychain*/
    func deleteAllSavedData() {
        _ = deleteValue(for: kKeychainUserRefreshToken) // Delete refresh token from Keychain
        _ = deleteValue(for: kKeychainUserAccessToken) // Delete access token from Keychain
        _ = deleteValue(for: kKeychainUserPassword) // Delete user password from Keychain
    }
    
}
