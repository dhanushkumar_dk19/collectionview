//
//  CVButton.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVButton: UIButton {
    
    // MARK: - Initializer methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customise()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customise()
    }
    
    // MARK: - Custom methods
    
    func customise() {
        // Customizing UI goes here
    }
    
}

