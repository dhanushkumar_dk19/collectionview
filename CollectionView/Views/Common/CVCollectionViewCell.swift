//
//  CVCollectionViewCell.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumCountLabel: UILabel!
    
    var albumPath: URL!
    var lastImageInAlbum: URL?
    var thumbnail: UIImage?
    static let identifier = "cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Custom methods
    
    func configureCell(with album: URL) {
        albumPath = album
        updateCell(for: albumPath)
    }
    
    func updateCell(for album: URL) {
        albumNameLabel.text = album.lastPathComponent
        albumCountLabel.text = albumContentsCount(in: album)
        lastImageInAlbum = getLastImage(in: album)
        bannerImage.image = lastImageInAlbum != nil ? UIImage(contentsOfFile: "\(lastImageInAlbum!.path)/thumbnail.jpg"): UIImage(named: "placeholder")
    }
    
    func albumContentsCount(in album: URL) -> String {
        let contentsInAlbum = album.getContentsOfDirectory()
        return String(contentsInAlbum.count)
    }
    
    func getLastImage(in album: URL) -> URL? {
        let contentsInAlbum = album.getContentsOfDirectory()
        return !contentsInAlbum.isEmpty ?  contentsInAlbum.last :  nil
    }
    
}


