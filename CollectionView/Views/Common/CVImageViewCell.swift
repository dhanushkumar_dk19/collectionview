//
//  CVImageViewCell.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 24/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVImageViewCell: UICollectionViewCell {

    static var identifier = "imageCell"
    @IBOutlet weak var viewAlbumImage: UIImageView!
    var width: CGFloat!
    
    // MARK: - Custom Method
    
    func updateCell(with image: URL) {
        viewAlbumImage.image =  UIImage(contentsOfFile: "\(image.path)/thumbnail.jpg")
        viewAlbumImage.clipsToBounds = true
    }
    
}
