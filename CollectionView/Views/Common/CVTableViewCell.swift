//
//  CVTableViewCell.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customise()
    }
    
    // MARK: - Custom methods
    
    func customise() {
        // Customizing UI goes here
    }
    
}
