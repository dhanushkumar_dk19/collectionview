//
//  CVTextView.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVTextView: UITextView {
    
    // MARK: - Initializer methods

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        customise()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customise()
    }

    // MARK: - Custom methods
    
    func customise() {
        // Customizing UI goes here
    }

}
