//
//  CVView.swift
//  CollectionView
//
//  Created by Dhanushkumar Kanagaraj on 23/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class CVView: UIView {
    
    // MARK: - Initializer methods

    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            return self.layer.cornerRadius = newValue
        }
    }
}
